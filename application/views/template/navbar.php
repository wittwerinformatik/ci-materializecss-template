<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- START NAVIGATION -->
<body>
<nav>
    <div class="nav-wrapper">
      <a href="#!" class="brand-logo">Logo</a>
      <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="sass.html">Sass</a></li>
        <li><a href="badges.html">Components</a></li>
        <li><a href="collapsible.html">Javascript</a></li>
        <li><a href="mobile.html">Mobile</a></li>
      </ul>
    </div>
</nav>

<ul class="sidenav" id="mobile-demo">
<li>
  <div class="user-view">
    <div class="background">
      <img src="https://picsum.photos/300/200" alt="Placeholder - please change">
    </div>
    <h5 class="white-text">Hier der Titel</h5>
    <br>
  </div>
</li>
  <li><a href="sass.html">Sass</a></li>
  <li><a href="badges.html">Components</a></li>
  <li><a href="collapsible.html">Javascript</a></li>
  <li><a href="mobile.html">Mobile</a></li>
</ul>
        
<br>
<!-- END NAVIGATION -->
<main>